class ArticlesController < ApplicationController
  before_action :find_article, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, except: [:index, :show]
  def index
  @articles = Article.all.order('created_at DESC').paginate(page: params[:page], per_page:3)
  end
  def show
  end
  def new
  @article = Article.new
  end
  def create
    @article = Article.new(article_params)
    if @article.save
      redirect_to @article, notice: "Article successfully Saved"
    else
      render 'new', notice: "Article Failed to Save"
    end
  end
  def edit
  end
  def update
    if @article.update article_params
      redirect_to @article, notice: "Article successfully Updated"
    else
      render 'edit', notice: "Article Failed to Update"
    end
  end
  def destroy
    @article.destroy
    redirect_to articles_path
  end

  private

  def article_params
    params.require(:article).permit(:title, :content, :slug)
  end

  def find_article
    @article = Article.friendly.find(params[:id])
  end
end
