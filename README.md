
Features : 
** Articles
          - Create / Edit / Destroy
          - Markdown
          - Syntax highlighting
          - Comments (Disqus)
** Projects
           -Create / Edit / Destroy
** Contact
          - Contact form
          - Sendgrid
** User (Devise)

Modeling our Data :
** Article
         - title:string
         - content:text
** Project
        - title:string
        - description:text
        - link:string
** User

Pages we need in our app :
-Home
-Articles#index
-Articles#Show
-Articles#New
-Articles#Edit
-Projects#index
-Projects#show
-Projects#New
-Projects#Edit
-Contact

